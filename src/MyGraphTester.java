import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;

import static org.junit.Assert.*;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;


/**
 * JUnit test for Graph Class developed in CSIS430 under Dr. David Hansen at
 * George Fox University.
 *
 * @Author Peyton Hanel
 */
public class MyGraphTester
{

   public static final String FIRST    = "first";
   public static final String SECOND   = "second";
   public static final String THIRD    = "third";
   public static final String FOURTH   = "fourth";
   public static final String FIFTH    = "fifth";
   public static final String SIXTH    = "sixth";
   public static final String SEVENTH  = "seventh";
   public static final String EIGHTH   = "eighth";
   public static final String NINTH    = "ninth";
   public static final String TENTH    = "tenth";
   public static final String ELEVENTH = "eleventh";
   public static final String TWELFTH  = "twelfth";

   //***************************************************************************
   // TESTS shortestPath()
   //***************************************************************************

   @Test
   public void testShortestPath_ForConnectedUndirectedGraph()
   {
      // tests shortest path of getConnectedUndirectedPath
      List<Graph.Edge<String>> shortestPath
         = getConnectedGraph(false).shortestPathBetween(FIRST, ELEVENTH);
      Iterator<Graph.Edge<String>> pathIterator = shortestPath.iterator();
      Graph.Edge<String> currentEdge;
      int totalWeight;

      // first edge should be FIRST -> SECOND
      currentEdge = pathIterator.next();
      totalWeight = currentEdge.getWeight();
      assertEquals(FIRST, currentEdge.getSource());
      assertEquals(SECOND, currentEdge.getDestination());

      // second edge should be SECOND -> SEVENTH
      currentEdge = pathIterator.next();
      totalWeight += currentEdge.getWeight();
      assertEquals(SECOND, currentEdge.getSource());
      assertEquals(SEVENTH, currentEdge.getDestination());

      // third edge should be SEVENTH -> ELEVENTH
      currentEdge = pathIterator.next();
      totalWeight += currentEdge.getWeight();
      assertEquals(SEVENTH, currentEdge.getSource());
      assertEquals(ELEVENTH, currentEdge.getDestination());

      // there should be no more edges and the total path length should be 9
      assertEquals(false, pathIterator.hasNext());
      assertEquals(9, totalWeight);
   }

   @Test
   public void testShortestPath_ForDirectedGraph()
   {
      assertNull(getConnectedGraph(true).shortestPathBetween(SECOND, FIRST));
   }

   @Test
   public void testShortestPath_ForUnconnectedUndirectedGraph()
   {
      assertNull(getUnconnectedGraph(false).shortestPathBetween(FIRST, FIFTH));
   }

   @Test
   public void testShortestPath_SourceIsDestination()
   {

      assertTrue(getConnectedGraph(false).shortestPathBetween(FIRST, FIRST).isEmpty());
   }

   //***************************************************************************
   // TESTS minimumSpanningTree()
   //***************************************************************************

   @Test
   public void testMinimumSpanningTree_WithSmallGraph()
   {
      Graph<String> smallGraph = new Graph<>(false);
      smallGraph.addVertex(FIRST);
      smallGraph.addVertex(SECOND);
      smallGraph.addVertex(THIRD);
      smallGraph.addEdge(FIRST, SECOND, 1);
      smallGraph.addEdge(SECOND, THIRD, 1);

      Graph<String> mst = smallGraph.minimumSpanningTree();
      assertEquals(3, mst.getVertices().size());
      assertNotNull(mst.getEdge(FIRST, SECOND));
      assertNotNull(mst.getEdge(SECOND, THIRD));
   }

   @Test
   public void testMinimumSpanningTree_WithConnectedUndirectedGraph()
   {
      int i = 0;
      for (Graph.Edge e
         : getConnectedGraph(false).minimumSpanningTree().getEdges())
      {
         i += e.getWeight();
      }
      assertEquals(17, i);
   }

   @Test
   public void testMinimumSpanningTree_WithDirectedGraph()
   {
      try {
         getConnectedGraph(true).minimumSpanningTree();
         // should be in catch block by now
         fail("Expected IllegalStateException. It is illegal to ask for the" +
            "minimum spanning tree of an undirected graph");
      }
      catch (IllegalStateException e) {/* success! */}
   }

   @Test
   public void testMinimumSpanningTree_WithUnconnectedGraph()
   {
      assertNull(getUnconnectedGraph(false).minimumSpanningTree());
   }

   //***************************************************************************
   // TESTS getOptimalTour()
   //***************************************************************************

//   @Test
//   public void testGetOptimalTour()
//      throws IOException, SAXException, ParserConfigurationException
//   {
//      Graph<String> eil101 = Graph.fromTSPFile(new FileInputStream("eil101.xml"));
//      assertTrue(eil101.pathLength(eil101.getOptimalTour(50, 0, 1000)) < 680);
//   }

   //***************************************************************************
   // TESTS pathLength()
   //***************************************************************************

   @Test
   public void testPathLength_PathOfOneVertex()
   {
      // a path of one vertex (that exists in the graph) should return a path
      // length of 0;
      assertEquals(0, getConnectedGraph(false).pathLength
         (new ArrayList<String>() {{add(FIRST);}}));
   }

   @Test
   public void testPathLength_PathOfOneVertexNotInGraph()
   {
      assertEquals(-1, getConnectedGraph(false).pathLength
         (new ArrayList<String>() {{add("not in graph");}}));
   }

   //***************************************************************************
   // PUBLIC STATIC HELPER METHODS
   //***************************************************************************

   /**
    * MST total weight is 17.
    *
    * Shortest path from FIRST to ELEVENTH:
    * FIRST -> SECOND -> SEVENTH -> ELEVENTH (total weight: 9)
    *
    * Shortest path from SEVENTH to FOURTH (undirected):
    * SEVENTH -> SIXTH -> FOURTH (total weight: 8)
    *
    * Shortest path from SEVENTH to FOURTH (directed):
    * SEVENTH -> NINTH -> TENTH -> EIGHTH -> FIFTH -> THIRD -> FOURTH
    * (total weight: 14)
    *
    * If directed, FIRST has no paths back to it. SECOND only has a path to it
    * from FIRST.
    */
   public static Graph<String> getConnectedGraph(boolean isDirected)
   {
      Graph<String> hardCodedGraph = new Graph<>(isDirected);

      hardCodedGraph.addVertex(FIRST);
      hardCodedGraph.addVertex(SECOND);
      hardCodedGraph.addVertex(THIRD);
      hardCodedGraph.addVertex(FOURTH);
      hardCodedGraph.addVertex(FIFTH);
      hardCodedGraph.addVertex(SIXTH);
      hardCodedGraph.addVertex(SEVENTH);
      hardCodedGraph.addVertex(EIGHTH);
      hardCodedGraph.addVertex(NINTH);
      hardCodedGraph.addVertex(TENTH);
      hardCodedGraph.addVertex(ELEVENTH);

      hardCodedGraph.addEdge(FIRST, SECOND, 3);
      hardCodedGraph.addEdge(FIRST, THIRD, 2);
      hardCodedGraph.addEdge(FIRST, SIXTH, 5);
      hardCodedGraph.addEdge(SECOND, SEVENTH, 3);
      hardCodedGraph.addEdge(THIRD, FOURTH, 1);
      hardCodedGraph.addEdge(THIRD, FIFTH, 2);
      hardCodedGraph.addEdge(FOURTH, FIFTH, 1);
      hardCodedGraph.addEdge(FOURTH, SIXTH, 6);
      hardCodedGraph.addEdge(FIFTH, EIGHTH, 4);
      hardCodedGraph.addEdge(SIXTH, SEVENTH, 2);
      hardCodedGraph.addEdge(SIXTH, EIGHTH, 2);
      hardCodedGraph.addEdge(SEVENTH, NINTH, 5);
      hardCodedGraph.addEdge(SEVENTH, ELEVENTH, 3);
      hardCodedGraph.addEdge(EIGHTH, TENTH, 1);
      hardCodedGraph.addEdge(NINTH, TENTH, 1);
      hardCodedGraph.addEdge(NINTH, ELEVENTH, 1);
      hardCodedGraph.addEdge(TENTH, ELEVENTH, 3);

      // adds for directed graphs
      hardCodedGraph.addEdge(ELEVENTH, SEVENTH, 3);
      hardCodedGraph.addEdge(TENTH, EIGHTH, 1);
      hardCodedGraph.addEdge(EIGHTH, FIFTH, 4);
      hardCodedGraph.addEdge(FIFTH, THIRD, 2);

      return hardCodedGraph;
   }

   /**
    * FIFTH is disconnected
    * @return
    */
   public static Graph<String> getUnconnectedGraph(boolean isDirected)
   {
      Graph<String> unconnectedGraph = new Graph<>(isDirected);

      unconnectedGraph.addVertex(FIRST);
      unconnectedGraph.addVertex(SECOND);
      unconnectedGraph.addVertex(THIRD);
      unconnectedGraph.addVertex(FOURTH);
      unconnectedGraph.addVertex(FIFTH);

      unconnectedGraph.addEdge(FIRST, SECOND, 3);
      unconnectedGraph.addEdge(SECOND, THIRD, 2);
      unconnectedGraph.addEdge(SECOND, FOURTH, 5);
      unconnectedGraph.addEdge(THIRD, FOURTH, 4);

      // if isDirected
      unconnectedGraph.addEdge(THIRD, SECOND, 2);
      unconnectedGraph.addEdge(SECOND, FIRST, 3);

      return unconnectedGraph;
   }

}
