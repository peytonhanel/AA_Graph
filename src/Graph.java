import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;


/**
 * A simple templated graph class. Does not allow loops, null vertexes, or
 * multiple edges between a specific source and destination. Each edge has with
 * it a non negative integer weight.
 *
 * @param <T> The type that is to be used as vertices within this graph.
 */
public class Graph<T>
{

   private static final int NO_EDGE = -1;

   // T (key) represents vertices, nested hashmap (value) is edges to other
   // vertices and the weights of each edge.
   private HashMap<T, HashMap<T, Integer>> _graphHashMap;
   private boolean _isDirected;


   /**
    * Constructs a new graph.
    *
    * @param isDirected Is this graph directed?
    */
   public Graph(boolean isDirected)
   {
      _isDirected = isDirected;
      _graphHashMap = new HashMap<>();
   }


   /**
    * Adds a new vertex to this graph with no edges connected to it.
    * Cannot add a null value.
    *
    * @param vertex The object to add to this graph as a vertex.
    * @throws IllegalArgumentException If vertex is null.
    */
   public void addVertex(T vertex) throws IllegalArgumentException
   {
      // precondition: vertex must not be null
      if (vertex == null) {
         throw new IllegalArgumentException("Cannot add null as a vertex");
      }
      // precondition: vertex must not already be in graph.
      if (_graphHashMap.containsKey(vertex)) {
         throw new IllegalArgumentException(vertex + " already exists as a" +
            " vertex in this graph.");
      }
      _graphHashMap.put(vertex, new HashMap<>());
   }


   /**
    * Adds a new edge between two vertices, originating at source and moving
    * towards destination. In an undirected graph, adding an edge will
    * automatically add a duplicate edge pointing in the opposite direction
    * (from destination to source). This is a simple graph, and adding an edge
    * when an edge exists will only update the weight.
    *
    * @param source The vertex the edge originates from.
    * @param destination The vertex the edge is pointing towards.
    * @param weight A numeric value associated with this edge. Must be
    *               non-negative.
    *
    * @throws IllegalArgumentException If the weight is negative, or if the
    * destination is the same as the source.
    * @throws NoSuchElementException If neither source nor destination exist
    * within this graph.
    */
   public void addEdge(T source, T destination, int weight)
      throws IllegalArgumentException, NoSuchElementException
   {
      // precondition: ensure that source and destination are vertices in this
      assertVertexExists(source);
      assertVertexExists(destination);

      //precondition: weight is a non negative number
      if (weight < 0) {
         throw new IllegalArgumentException("The weight must be non-negative.");
      }
      // precondition: destination is not identical to source (the edge being
      // added is not a loop)
      if (source.equals(destination)) {
         throw new IllegalArgumentException("This is a simple graph and loops" +
            " cannot be added.");
      }

      // adds the destination to the HashMap associated with source, thus
      // creating an edge between the two vertices.
      _graphHashMap.get(source).put(destination, weight);

      // if undirected, add a edge in the opposite direction
      if (!_isDirected) {
         _graphHashMap.get(destination).put(source, weight);
      }
   }


   /**
    * Returns a list containing the shortest path between any two vertices in
    * this graph.
    *
    * @param source The start of the path.
    * @param destination The end of the path.
    *
    * @return A list of edges that makes up the shortest path between the source
    * and the destination. Null if no path exists.
    *
    * @throws NoSuchElementException If the source or destination are not
    * vertices in this graph.
    */
   public List<Edge<T>> shortestPathBetween(T source, T destination)
      throws NoSuchElementException
   {
      // precondition: ensure that source and destination are vertices in this
      assertVertexExists(source);
      assertVertexExists(destination);

      // keys are visited vertices (destinations), values are source vertices
      HashMap<T, T> visitedVertices = new HashMap<>();
      Queue<Edge<T>> edgesToConsider = new PriorityQueue<>();
      LinkedList<Edge<T>> shortestPath = null;
      Edge<T> currentShortestEdge = null;
      T currentVertex = source; // the vertex we are currently visiting
      int currentPathLength = 0; // weight of path from source to currentVertex

      // adds the source to visitedVertices so we don't visit it again
      visitedVertices.put(source, source);

      // precondition: source is not destination
      if (!source.equals(destination))
      {
         // Finds shortest path from source to destination using Dijkstra's
         // shortest path algorithm. Iterates through necessary vertices,
         // considering edges connected to visited vertices, and saving the
         // edges with the lowest weights from the source.
         do {
            // add every edge connected to currentVertex to edgesToConsider
            for (T edge : _graphHashMap.get(currentVertex).keySet()) {
               edgesToConsider.add(new Edge<>(currentVertex, edge,
                  _graphHashMap.get(currentVertex).get(edge)
                     + currentPathLength));
            }
            // finds the next shortest edge with an unvisited destination
            currentShortestEdge = edgesToConsider.poll();
            while (!edgesToConsider.isEmpty() && visitedVertices.containsKey
               (currentShortestEdge.getDestination()))
            {
               currentShortestEdge = edgesToConsider.poll();
            }
            // set currentVertex to destination, keep track of it and it's
            // source so a path can be built later.
            if (currentShortestEdge != null
               && !visitedVertices.containsKey
                  (currentShortestEdge.getDestination()))
            {
               visitedVertices.put(currentShortestEdge.getDestination(),
                  currentShortestEdge.getSource());
               currentVertex = currentShortestEdge.getDestination();
               currentPathLength = currentShortestEdge.getWeight();
            }
         }// do{}
         while (!currentVertex.equals(destination)
            && !edgesToConsider.isEmpty());

         // constructs the shortest path from visitedVertices
         if (currentVertex.equals(destination)) {
            shortestPath = new LinkedList<>();
            while (!currentVertex.equals(source)) {
               shortestPath.addFirst(new Edge<>
                  (visitedVertices.get(currentVertex), currentVertex,
                     getEdgeWeight(visitedVertices.get(currentVertex),
                        currentVertex)));
               currentVertex = visitedVertices.get(currentVertex);
            }
         }
      }// if()
      // return empty list if source is destination
      else {
         shortestPath = new LinkedList<>();
      }
      return shortestPath;
   }


   /**
    * Returns a graph representing a minimum spanning tree of this graph, or
    * null if there is no minimum spanning tree. It is illegal to call this
    * method for directed graphs.
    *
    * @return A minimum spanning tree of this graph, or null if there is none.
    *
    * @throws IllegalStateException If this is a directed graph.
    */
   public Graph<T> minimumSpanningTree() throws IllegalStateException
   {
      // precondition: this is not a directed graph
      if (_isDirected) {
         throw new IllegalStateException("Graph is a directed graph.");
      }

      HashSet<T> visitedVertices = new HashSet<>();
      Queue<Edge<T>> edgesToConsider = new PriorityQueue<>();
      Graph<T> minimumSpanningTree = null;
      Edge<T> currentEdge = null;
      List<T> allVertices = getVertices();
      T currentVertex;

      // precondition: graph musn't be empty
      if (!allVertices.isEmpty())
      {
         minimumSpanningTree = new Graph<>(false);
         currentVertex = allVertices.get(0);
         visitedVertices.add(currentVertex);
         minimumSpanningTree.addVertex(currentVertex);

         // Creates a minimumSpanningTree of this graph using Prim's algorithm.
         // Iterates through every vertex, considering all edges connected to
         // visited vertices, and adds the ones with the lowest weights
         for (int i = 0; i < allVertices.size(); i++)
         {
            // adds all edges of currentVertex to edgesToConsider, tracks
            // cumulative weight of that edge from the starting vertex
            for (T edge : _graphHashMap.get(currentVertex).keySet()) {
               edgesToConsider.add(new Edge<>(currentVertex, edge,
                  _graphHashMap.get(currentVertex).get(edge)));
            }
            // finds the next shortest edge with an unvisited destination
            currentEdge = edgesToConsider.poll();
            while (!edgesToConsider.isEmpty()
               && visitedVertices.contains(currentEdge.getDestination()))
            {
               currentEdge = edgesToConsider.poll();
            }
            // adds currentEdge to minimumSpanningTree, currentVertex to
            // visitedVertices, currentVertex now currentEdge's destination
            if (currentEdge != null &&
               !visitedVertices.contains(currentEdge.getDestination()))
            {
               minimumSpanningTree.addVertex(currentEdge.getDestination());
               minimumSpanningTree.addEdge(currentEdge.getSource(),
                  currentEdge.getDestination(),
                  getEdgeWeight(currentEdge.getSource(),
                     currentEdge.getDestination()));

               visitedVertices.add(currentEdge.getDestination());
               currentVertex = currentEdge.getDestination();
            }
         }
         // checks if graph is unconnected, meaning not all vertices couldnt be
         // added to minimumSpanningTree, so return null
         if (minimumSpanningTree.getVertices().size() < getVertices().size()) {
            minimumSpanningTree = null;
         }
      }
      return minimumSpanningTree;
   }


   /**
    * Returns an optimal tour of every vertex in this graph (assumes the graph
    * is fully connected and undirected). This is an implementation of the
    * inver-over operator for the TSP, which is a unary operator based on
    * simple inversion that considers the surrounding population.
    * Citation: http://dl.acm.org/citation.cfm?id=668606
    */
   public List<T> getOptimalTour()
   {
      // these values are the default because they are found in the citation.
      // They do not give optimal results. It is recommended to use
      // (50, 0, 1000).
      final int POPULATION    = 100;
      final float PROBABILITY = 0.02f;
      final int ITERATIONS    = 10;

      return getOptimalTour(POPULATION, PROBABILITY, ITERATIONS);
   }


   /**
    * Returns an optimal tour of every vertex in this graph (assumes the graph
    * is fully connected and undirected). This is an implementation of the
    * inver-over operator for the TSP, which is a unary operator based on
    * simple inversion that considers the surrounding population.
    * Citation: http://dl.acm.org/citation.cfm?id=668606
    *
    * @param populationSize The number of random tours that will be generated
    *                       and manipulated.
    * @param inversionProbability The probability of generating a random
    *                             inversion in a tour without the influence of
    *                             other tours in the population.
    * @param terminationIterations The number of attempts to generate the
    *                              shortest tour before returning.
    *
    * @return A list of vertices being an optimal tour of every vertex in this
    * graph.
    */
   public List<T> getOptimalTour(int populationSize,
      float inversionProbability, int terminationIterations)
   {
      ArrayList<ArrayList<T>> population = new ArrayList<>();
      Random random = new Random();
      ArrayList<T> bestTour = null;
      long bestTourLength = NO_EDGE;

      // initialize the population
      for (int i = 0; i < populationSize; i++)
      {
         // creates a new individual and shuffles the vertices
         ArrayList<T> newIndividual = (ArrayList<T>) getVertices();
         Collections.shuffle(newIndividual);
         population.add(newIndividual);

         // finds the best individual (has shortest path length)
         long newIndividualPathLength = pathLength(newIndividual);
         if (newIndividualPathLength < bestTourLength || i == 0) {
            bestTour = newIndividual;
            bestTourLength = newIndividualPathLength;
         }
      }
      // Genetic algorithm that finds an optimal tour in this graph. Runs until
      // bestTour has not changed for a number of iterations equal to
      // terminationIterations. Iterates through each individual in population,
      // copying individual and mutating it by inversing a set of vertices
      // between two random vertices (currentVertex and randomVertex).
      int terminalCounter = 0;
      while (terminalCounter < terminationIterations) {
         for(int i = 0; i < population.size(); i++)
         {
            // copies the current individual and gets a random vertex from it
            ArrayList<T> child = new ArrayList<>(population.get(i));
            int vertexIndex = random.nextInt(child.size());
            int nextIndex = (vertexIndex + 1) % child.size();
            int previousIndex = decrementAndWrap(vertexIndex, child.size() - 1);
            T currentVertex = child.get(vertexIndex);

            // inverse a section in child between currentVertex and randomVertex
            // until a vertex adjacent to currentVertex equals randomVertex
            boolean foundGoodAdjacentVertex = false;
            while (!foundGoodAdjacentVertex)
            {
               // gets a different random vertex from child
               T randomVertex = getRandomVertex(inversionProbability,
                  currentVertex, i, child, population);

               // exit loop if randomVertex is adjacent to currentVertex
               if (child.get(nextIndex).equals(randomVertex)
                  || child.get(previousIndex).equals(randomVertex))
               {
                  foundGoodAdjacentVertex = true;
               }
               else
               {
                  inverseChild(child, nextIndex, child.indexOf(randomVertex));

                  // randomVertex is now the vertex at nextIndex in child. Set
                  // currentVertex to randomVertex
                  currentVertex = randomVertex;
                  vertexIndex = nextIndex;
                  nextIndex = (vertexIndex + 1) % child.size();
                  previousIndex = decrementAndWrap(vertexIndex,
                     child.size() - 1);
               }
            }
            // compare path length of child with parent and replace if shorter.
            // checks if child is the best tour so far
            long childPathLength = pathLength(child);
            if (childPathLength <= pathLength(population.get(i))) {
               population.set(i, child);
               if (childPathLength < bestTourLength) {
                  bestTourLength = childPathLength;
                  bestTour = population.get(i);
                  terminalCounter = NO_EDGE;
               }
            }
         }// for()
         terminalCounter++;
      }// while()

      // completes the tour
      bestTour.add(bestTour.get(0));
      return bestTour;
   }


   /**
    * Gets a random vertex from either child or from a random individual in the
    * population.
    *
    * @param inversionProbability The probability to get the vertex from child
    * @param currentVertex A vertex being used in getOptimalTour that should not
    *                      be selected for the randomVertex.
    * @param parentIndex The index of child's parent in the population.
    * @param child The list to perform the inversion in.
    * @param population The population of individuals.
    *
    * @return A random vertex found in child or in a random individual.
    */
   private T getRandomVertex(float inversionProbability, T currentVertex,
      int parentIndex, ArrayList<T> child, ArrayList<ArrayList<T>> population)
   {
      Random random = new Random();
      T randomVertex;

      if (random.nextFloat() <= inversionProbability) {
         do {
            randomVertex = child.get(random.nextInt(child.size()));
         }
         while (currentVertex.equals(randomVertex));
      }
      // gets a random vertex from another individual
      else {
         // gets a different random individual from population
         int indexOfIndividual;
         do {
            indexOfIndividual = random.nextInt(population.size());
         }
         while (indexOfIndividual == parentIndex);
         ArrayList<T> randomIndividual
            = population.get(indexOfIndividual);

         // sets randomVertex to the next vertex after currentVertex
         randomVertex = randomIndividual.get(
            (randomIndividual.indexOf(currentVertex) + 1)
               % randomIndividual.size());
      }

      return randomVertex;
   }


   /**
    * Inverses a section in child starting at lowerBound and ending at
    * upperBound.
    *
    * @param child The list to perform the inversion in.
    * @param lowerBound The index to start the inversion.
    * @param upperBound The index to end the inversion.
    */
   private void inverseChild(ArrayList<T> child, int lowerBound, int upperBound)
   {
      // inverse the section between lowerBound and upperBound
      boolean isInversing = true;
      while (isInversing)
      {
         // swaps the vertices at indexes lowerBound and upperBound
         T referenceToVertexAtj = child.get(lowerBound);
         child.set(lowerBound, child.get(upperBound));
         child.set(upperBound, referenceToVertexAtj);

         // increment lowerBound, decrement upperBound, wrap if they go out of
         // bounds, quit loop if they pass each other or are equal.
         lowerBound = (lowerBound + 1) % child.size();
         if (lowerBound == upperBound) {
            isInversing = false;
         }
         upperBound = decrementAndWrap(upperBound, child.size() - 1);
         if (lowerBound == upperBound) {
            isInversing = false;
         }
      }

   }


   /**
    * Decrements a number. If it becomes less than 0, set it equal to bounds.
    *
    * @param toDecrement The number to decrement.
    * @param bounds The upper bound that toDecrement will equal if it becomes
    *               less than 0.
    * @return the decremented number, or bounds if it was decremented below 0.
    */
   private int decrementAndWrap(int toDecrement, int bounds)
   {
      if (--toDecrement < 0) {
         toDecrement = bounds;
      }
      return toDecrement;
   }


   /**
    * Returns the the total weight of a path (being a list of adjacent vertices)
    * that can be found in this graph. Returns -1 if it is an invalid path.
    *
    * @param path The list of vertices. Each vertex listed is assumed to have a
    *             shared edge with itself both of its adjacent vertices. A list
    *             of one vertex is acceptable and returns 0.
    *
    * @return The total weight of every edge in this path, 0 if path has one
    * vertex that exists in the graph, -1 if the path is invalid (i.e. no edge
    * exists between any two adjacent vertices).
    */
   public long pathLength(List<T> path)
   {
      long pathLength = NO_EDGE;
      int lastWeight = 0;

      // gets the weights from edges between each vertex in path and adds them
      // to pathLength
      if (path != null && path.size() >= 1)
      {
         Iterator<T> iterator = path.iterator();
         T previousVertex;
         T currentVertex = iterator.next();

         // iterates thru path, checks for edges and adds weights to pathLength
         if (_graphHashMap.containsKey(currentVertex))
         {
            pathLength = 0;
            while (iterator.hasNext() && lastWeight != NO_EDGE) {
               previousVertex = currentVertex;
               currentVertex = iterator.next();
               lastWeight = getEdgeWeight(previousVertex, currentVertex);
               pathLength += lastWeight;
            }
            // loop exited because no edge between two vertices
            if (lastWeight == NO_EDGE) {
               pathLength = NO_EDGE;
            }
         }
      }
      return pathLength;
   }


   /**
    * Checks if an edge exists between two vertices.
    *
    * @param source The vertex this edge would be originating from.
    * @param destination The vertex this edge would be going toward.
    * @return True if the edge exists.
    */
   public boolean edgeExists(T source, T destination)
   {
      return getEdgeWeight(source, destination) != NO_EDGE;
   }


   /**
    * Returns the weight of an edge between two vertices. For directed graphs,
    * swapping the source and the destination may return different values.
    * Otherwise it will return the same value for each method call. Returns -1
    * if no edge is present.
    *
    * @param source The vertex that the edge originates from.
    * @param destination The vertex that the edge goes to.
    * @return A non-negative integer representing the weight of the edge. Will
    * return -1 if there is no edge present.
    */
   public int getEdgeWeight(T source, T destination)
   {
      int weight = NO_EDGE;

      // gets the weight between the source and the destination.
      if (_graphHashMap.containsKey(source)
         && _graphHashMap.get(source).containsKey(destination))
      {
         weight = _graphHashMap.get(source).get(destination);
      }

      return weight;
   }


   /**
    * Returns an edge from source to destination. Null if the specified edge
    * doesn't exist in this graph.
    *
    * @param source the vertex that is the start of the edge.
    * @param destination the vertex that the edge points towards.
    *
    * @return An edge from source to destination. Null if edge doesn't exist.
    */
   public Edge<T> getEdge(T source, T destination)
   {
      // checks if source is a vertex in this graph and if it has an edge to
      // destination. If so, will return an edge from source to destination.
      Edge<T> foundEdge = null;
      if (_graphHashMap.get(source) != null
         && _graphHashMap.get(source).containsKey(destination))
      {
         foundEdge = new Edge<>(source, destination,
            _graphHashMap.get(source).get(destination));
      }
      return foundEdge;
   }


   /**
    * Returns a list of every edge in this graph.
    *
    * @return A list of every edge in this graph.
    */
   public List<Edge<T>> getEdges()
   {
      HashSet<T> visitedVertices = new HashSet<>();
      List<Edge<T>> foundEdges = new LinkedList<>();

      // for each vertex, for each edge of that vertex, add edge to foundEdges
      for (T currentVertex : getVertices()) {
         for (T destination : _graphHashMap.get(currentVertex).keySet()) {

            // for undirected graphs, edges will not be added if the current
            // destination has been visited (i.e. it was once a currentVertex)
            // for directed graphs, visitedVertices will always be empty
            if (!visitedVertices.contains(destination)) {
               foundEdges.add(new Edge<>(currentVertex, destination,
                  _graphHashMap.get(currentVertex).get(destination)));
            }
         }
         // for undirected graphs, once we've added ever edge of this vertex,
         // mark it as visited so duplicate edges are not added
         if (!_isDirected) {
            visitedVertices.add(currentVertex);
         }
      }
      return foundEdges;
   }


   /**
    * Returns a list of all the vertices in this graph.
    *
    * @return a list of all the vertices in this graph.
    */
   public List<T> getVertices()
   {
      // makes an arraylist and adds all the vertices to it before returning
      return new ArrayList<>(_graphHashMap.keySet());
   }


   /**
    * Removes a vertex from this graph, as well as every edge connected to it.
    *
    * @param vertex The vertex to remove.
    * @throws NoSuchElementException If the vertex does not exist in this graph.
    */
   public void removeVertex(T vertex) throws NoSuchElementException
   {
      // precondition: ensures that this graph contains vertex.
      assertVertexExists(vertex);

      // for undirected graph, removes vertex from every other vertex that
      // includes it as an edge before removing it from _graphHashMap.
      if (!_isDirected) {
         for (T edge : _graphHashMap.get(vertex).keySet()) {
            _graphHashMap.get(edge).remove(vertex);
         }
         _graphHashMap.remove(vertex);
      }
      // for directed graph, ensure every vertex in the graph does not have
      // vertex as an edge before removing vertex from _graphHashMap.
      else {
         for (T currentVertex : getVertices()) {
            _graphHashMap.get(currentVertex).remove(vertex);
         }
         _graphHashMap.remove(vertex);
      }
   }


   /**
    * Removes an edge from this graph.
    *
    * @param source The origin of this edge.
    * @param destination The vertex this edge goes to.
    * @throws NoSuchElementException If the source or destination are not
    * vertices in this graph.
    */
   public void removeEdge(T source, T destination)
      throws NoSuchElementException
   {
      // precondition: source is a vertex in this graph
      if (!edgeExists(source, destination)) {
         throw new NoSuchElementException("There is no edge between " + source
            + " and " + destination + ".");
      }

      _graphHashMap.get(source).remove(destination);

      // directed graphs must have the opposite edge removed as well
      if (!_isDirected) {
         _graphHashMap.get(destination).remove(source);
      }
   }


   /**
    * Returns a human-readable string of information regarding this graph.
    * Included: if it's directed, a list of all vertices, and a list of edges.
    *
    * @return a string with information regarding this graph.
    */
   public String toString()
   {
      return _graphHashMap.toString();
   }


   /**
    * Asserts that a specified vertex exists in this graph.
    *
    * @param vertex The vertex to look for in the graph.
    *
    * @throws NoSuchElementException If the vertex doesn't exist in the graph.
    */
   private void assertVertexExists(T vertex)
      throws NoSuchElementException
   {
      if (!_graphHashMap.containsKey(vertex)) {
         throw new NoSuchElementException(vertex + " does not exist as a" +
            " vertex in this graph.");
      }
   }


   /**
    * Constructs and returns a graph from a CVSFile. Assumes that the file has
    * the following format for specifying vertices and edges:
    *
    * <# of vertices>
    * <first vertex>
    * <second vertex>
    * ...
    * <nth vertex>
    * <# of edges>
    * <source>,<destination>,<weight>
    * <source>,<destination>,<weight>
    * ...
    *
    * @param isDirected Are the edges in this graph directed?
    * @param inputFile The file to read from for constructing this graph.
    *
    * @return A new graph that has been populated with vertices and edges based
    * on what was specified in inputFile.
    *
    * @throws IOException If anything in the file has incorrect formatting, or
    * if there is anything at all wrong with the file.
    */
   public static Graph<String> fromCSVFile
   (boolean isDirected, Scanner inputFile)
      throws IOException
   {
      Graph<String> graphFromFile = new Graph<>(isDirected);

      // generates a graph from csv file
      try
      {
         // adds every vertex listed in file to the graph
         inputFile.useDelimiter("\r\n|\n");
         int verticesListLength = inputFile.nextInt();
         for (int i = 0; i < verticesListLength; i++) {
            graphFromFile.addVertex(inputFile.next());
         }
         // adds every edge listed in the file to the graph
         inputFile.useDelimiter(",|\r\n|\n");
         int edgesListLength = inputFile.nextInt();
         for (int i = 0; i < edgesListLength; i++) {
            graphFromFile.addEdge(inputFile.next(), inputFile.next(),
               inputFile.nextInt());
         }
      }
      // Expected exceptions will be rethrown as an IOException
      catch (NoSuchElementException e) {
         throw new IOException("Incorrect file format.");
      }

      return graphFromFile;
   }


   /**
    * Construct an undirected graph from an XML encoded TSP file
    *
    * @param inputFile an XML encoded TSP file from
    *        <a href="http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/">
    *        http://comopt.ifi.uni-heidelberg.de/software/TSPLIB95/</a>
    *
    * @return graph populated from the file
    *
    * @throws ParserConfigurationException, SAXException, IOException if
    *         the file doesn't conform to the specification
    */
   public static Graph<String> fromTSPFile(InputStream inputFile)
      throws ParserConfigurationException, SAXException, IOException
   {
      /**
       * The Handler for SAX Parser Events.
       *
       * This inner-class extends the default handler for the SAX parser
       * to construct a graph from a TSP file
       *
       * @see org.xml.sax.helpers.DefaultHandler
       */
      class TSPGraphHandler extends DefaultHandler
      {
         // Instantiate an undirected graph to populate; vertices are
         // integers though we treat them as strings for extension to other
         // similarly-formed files representing, say, GFU.
         private Graph<String> _theGraph = new Graph<>(false);

         private final int NO_WEIGHT = -1;
         // As we parse we need to keep track of when we've seen
         // vertices and edges
         private int _sourceVertexNumber = 0;
         private String _destinationVertexName = null;
         private String _sourceVertexName = null;
         private int _edgeWeight = NO_WEIGHT;
         private boolean _inEdge = false;


         /**
          * Parser has seen an opening tag
          *
          * For a <pre>vertex</pre> tag we add the vertex to the graph
          * the first time we encounter it.
          * For an <pre>edge</pre> tag we remember the weight of the edge.
          *
          * {@inheritDoc}
          */
         @Override
         public void startElement(String uri, String localName,
            String qName, Attributes attributes)
            throws SAXException
         {
            // We only care about vertex and edge elements
            switch (qName)
            {
               case "vertex":
                  // See if the vertices are named; if so, use the
                  // name, otherwise use the number
                  _sourceVertexName = attributes.getValue("name");
                  if (_sourceVertexName == null) {
                     _sourceVertexName = Integer.toString(_sourceVertexNumber);
                  }
                  // If is vertex 0 then it's the first time we're seeing it;
                  // add it to the graph. Other vertices will be added
                  // as we encounter their edges
                  if (_sourceVertexNumber == 0) {
                     _theGraph.addVertex(_sourceVertexName);
                  }
                  break;

               case "edge":
                  // Edges have the destination vertex within so
                  // indicate that we're inside an edge so that the
                  // character-parsing method below will grab the
                  // destination vertex as it encounters it
                  _inEdge = true;
                  // The weight of the edge is given by the "cost"
                  // attribute
                  _edgeWeight = (int) Double.parseDouble
                     (attributes.getValue("cost"));
                  break;

               default: // ignore any other opening tag
            }
         }


         /**
          * Parser has seen a closing tag.
          *
          * For a <pre>vertex</pre> tag we increment the vertex number
          * to keep track of which vertex we're parsing.
          * For a <pre>edge</pre> tag we use the number of the edge and
          * the weight we saw in the opening tag to add an edge to the
          * graph.
          *
          * {@inheritDoc}
          */
         @Override
         public void endElement(String uri, String localName,
            String qName)
            throws SAXException
         {
            // Again, we only care about vertex and edge tags
            switch (qName) {

               case "vertex":
                  // End of a vertex so we're moving on to the next
                  // source vertex number
                  _sourceVertexNumber++;
                  // Clear out the name so we don't inherit it in some
                  // mal-formed entry later
                  _sourceVertexName = null;
                  break;

               case "edge":
                  // We've finished an edge so we have collected all the
                  // information needed to add an edge to the graph
                  _inEdge = false;
                  // If this is the first set of edges (i.e., we're on
                  // the first source vertex) then this is the first
                  // time we've seen the destination vertex; add it to
                  // the graph
                  if (_sourceVertexNumber == 0) {
                     _theGraph.addVertex(_destinationVertexName);
                  }
                  // Should now be safe to add an edge between the
                  // source and destination
                  _theGraph.addEdge(_sourceVertexName,
                     _destinationVertexName, _edgeWeight);
                  // Clear out the attributes of this edge so we don't
                  // accidentally inherit them should we parse a
                  // mal-formed edge entry later
                  _destinationVertexName = null;
                  _edgeWeight = NO_WEIGHT;
                  break;

               default: // ignore any other closing tag
            }
         }


         /**
          * Parser has seen a string of characters between opening and
          * closing tag. The only characters we care about occur within
          * an <pre>edge</pre> tag and are the destination vertex.
          *
          * {@inheritDoc}
          */
         @Override
         public void characters(char[] ch, int start, int length)
            throws SAXException
         {
            // If we're within an edge, then this string of characters
            // is the number of the destination vertex for this edge.
            // Remember the destination vertex
            if (_inEdge) {
               _destinationVertexName = new String(ch, start, length);
            }
         }


         /**
          * @return the graph constructed
          */
         Graph<String> getGraph()
         {
            return _theGraph;
         }

      } // TSPHandler
      // Create a handler and use it for parsing
      TSPGraphHandler tspHandler = new TSPGraphHandler();

      // Here's where we do the actual parsing using the local class
      // defined above. Give the parser an instance of the class above
      // as the handler and parse away!
      SAXParserFactory.newInstance().newSAXParser().parse(inputFile, tspHandler);

      // Graph should now be populated, return it
      return tspHandler.getGraph();

   }


   /**
    * Represents a connection between two vertices. Associated with it is a
    * numerical weight. This is a directed edge, having a source and a
    * destination.
    *
    * @param <E> The type that is represented as vertices.
    */
   public static class Edge<E> implements Comparable<Edge<E>>
   {

      private E _source;
      private E _destination;
      private int _weight;


      /**
       * Constructs a new directed edge.
       *
       * @param source The vertex this edge originates at.
       * @param destination The vertex this edge points to.
       * @param weight The numerical value associated with this edge.
       */
      public Edge(E source, E destination, int weight)
      {
         _source = source;
         _destination = destination;
         _weight = weight;
      }


      /**
       * Returns the weight of this edge.
       *
       * @return This edge's weight.
       */
      public int getWeight()
      {
         return _weight;
      }


      /**
       * Returns the vertex this edge originates at.
       *
       * @return The vertex this edge originates at.
       */
      public E getSource()
      {
         return _source;
      }


      /**
       * Returns the vertex that this edge is pointing towards.
       *
       * @return The vertex that this edge is pointing towards.
       */
      public E getDestination()
      {
         return _destination;
      }


      /**
       * Returns a string representation of this edge.
       *
       * @return A string representation of this edge.
       */
      public String toString()
      {
         return _source + ", " + _destination + " (" + _weight + ")";
      }


      /**
       * Compares this object with the specified object for order. Returns a
       * negative integer, zero, or a positive integer as this object is less
       * than, equal to, or greater than the specified object.
       * <p>
       * <p>The implementor must ensure <tt>sgn(x.compareTo(y)) ==
       * -sgn(y.compareTo(x))</tt> for all <tt>x</tt> and <tt>y</tt>.  (This
       * implies that <tt>x.compareTo(y)</tt> must throw an exception iff
       * <tt>y.compareTo(x)</tt> throws an exception.)
       * <p>
       * <p>The implementor must also ensure that the relation is transitive:
       * <tt>(x.compareTo(y)&gt;0 &amp;&amp; y.compareTo(z)&gt;0)</tt> implies
       * <tt>x.compareTo(z)&gt;0</tt>.
       * <p>
       * <p>Finally, the implementor must ensure that <tt>x.compareTo(y)==0</tt>
       * implies that <tt>sgn(x.compareTo(z)) == sgn(y.compareTo(z))</tt>, for
       * all <tt>z</tt>.
       * <p>
       * <p>It is strongly recommended, but <i>not</i> strictly required that
       * <tt>(x.compareTo(y)==0) == (x.equals(y))</tt>.  Generally speaking, any
       * class that implements the <tt>Comparable</tt> interface and violates
       * this condition should clearly indicate this fact.  The recommended
       * language is "Note: this class has a natural ordering that is
       * inconsistent with equals."
       * <p>
       * <p>In the foregoing description, the notation
       * <tt>sgn(</tt><i>expression</i><tt>)</tt> designates the mathematical
       * <i>signum</i> function, which is defined to return one of <tt>-1</tt>,
       * <tt>0</tt>, or <tt>1</tt> according to whether the value of
       * <i>expression</i> is negative, zero or positive.
       *
       * @param o the object to be compared.
       * @return a negative integer, zero, or a positive integer as this object
       * is less than, equal to, or greater than the specified object.
       * @throws NullPointerException if the specified object is null
       * @throws ClassCastException   if the specified object's type prevents it
       *                              from being compared to this object.
       */
      @Override
      public int compareTo(Edge<E> o)
      {
         return getWeight() - o.getWeight();
      }
   }
}
