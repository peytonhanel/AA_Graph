/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.List;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.NoSuchElementException;
import static org.junit.Assert.*;

/**
 * A Unit Test for a Simple Graph Class that can be Directed or Undirected
 * @author thomas
 */
public class TesterFromThomas 
{
    private static final boolean IS_DIRECTED = true;
    Graph<String> _testDirectedGraph;
    Graph<String> _testUndirectedGraph;
    Graph<String> pGraph;
    Graph<String> myGraph;
    Graph<String> badGraph;
    
    String a = "A";
    String b = "B";
    String c = "C";
    
    @Before
    public void setUp() 
    {
        _testDirectedGraph = new Graph<>(IS_DIRECTED);
        _testUndirectedGraph = new Graph<>(!IS_DIRECTED);
        pGraph = new Graph<>(!IS_DIRECTED);
        badGraph = new Graph<>(IS_DIRECTED);
        myGraph = new Graph<>(!IS_DIRECTED);
        
    }
    
    
    @Test
    public void getVerticesTest() throws Exception 
    {
        // Populates graph before we begin testing
        testPopulateGraph();
        List<String> a_list = _testDirectedGraph.getVertices();
        List<String> b_list = _testDirectedGraph.getVertices();
        List<String> c_list = _testDirectedGraph.getVertices();
        List<String> d_list = _testDirectedGraph.getVertices();
        
        // Assures that returned list is identical
        assertTrue(a_list.equals(b_list));
        assertTrue(b_list.equals(c_list));
        assertTrue(c_list.equals(a_list));
        assertTrue(d_list.equals(c_list));
        
        // Removes vertices from _testDirectedGraph
        _testDirectedGraph.removeVertex(a);
        _testDirectedGraph.removeVertex(b);
        _testDirectedGraph.removeVertex(c);
    }
    
    
    @Test
    public void getEdgeWeightTest() throws Exception 
    {
        testPopulateGraph();
        
        assertTrue(_testDirectedGraph.getEdgeWeight(null, null) == -1);
    }
    
    
    @Test
    public void edgeExistsTest() throws Exception 
    {
        testPopulateGraph();
        
        // Tests that undirected graph does in fact create edges both ways
        assertTrue(_testUndirectedGraph.edgeExists(b, a));
        
        _testUndirectedGraph.removeEdge(a, b);
        
        // Checks that a deletion of the sister edge, triggers the deletion
        // of this edge
        assertFalse(_testUndirectedGraph.edgeExists(b, a));
    }
    
    
    @Test
    public void addVertexTest() throws Exception 
    {
        testPopulateGraph();
        
        // If duplicate vertices or null vertices can be added to the
        // graph, fail...
        try
        {
            _testUndirectedGraph.addVertex(a);
            
            fail("If we got this far, then duplicate vertices can be added...");
        }
        catch (IllegalArgumentException iae) {}
        
        try
        {
            _testUndirectedGraph.addVertex(null);
            
            fail("If we got this far, then null vertices can be added...");
        }
        catch (IllegalArgumentException iae) {}
    }
    
    
    @Test
    public void removeVertexTest() throws Exception 
    {
        testPopulateGraph();
        
        try
        {
            _testDirectedGraph.removeVertex(null);
            
            fail("If we got here, we successfully deleted a vertex"
                    + "that doesn't exist. Bad news.");
        }
        catch(NoSuchElementException nse) {}
        
        _testDirectedGraph.removeVertex(b);
        
        // PICK UP HERE...
        assertFalse(_testDirectedGraph.edgeExists(a, b));
        assertFalse(_testDirectedGraph.edgeExists(b, c));
    }
    
    
    @Test
    public void addEdgeTest() throws Exception 
    {
        // Populates graph with a single vertex and no edges
        testPopulateGraph();
        
        try 
        {
            _testDirectedGraph.addEdge(a, a, 1);
            _testUndirectedGraph.addEdge(a, a, 1);
            
            // If an Illegal ArgumentException is not thrown, then the graph
            // allows loops, which are explicitly prohibited in the spec.
            fail("If we got here, the graph allows loops. This is bad news.");
        }
        catch (IllegalArgumentException iae) {} // Success
    }
    
    
    @Test
    public void removeEdgeTest() throws Exception 
    {
        testPopulateGraph();
        
        try
        {
            _testDirectedGraph.removeEdge(null, null);
            
            fail("If we got here, then we removed an edge that"
                    + "doesn't exist. Bad News.");
        }
        catch (NoSuchElementException nse) {}
        
        _testDirectedGraph.removeEdge(a, b);
        
        assertFalse(_testDirectedGraph.edgeExists(a, b));
        
        _testDirectedGraph.addEdge(a, b, 0);
        
        assertTrue(_testDirectedGraph.edgeExists(a, b));
        
        _testDirectedGraph.removeEdge(a, b);
        
        assertFalse(_testDirectedGraph.edgeExists(a, b));
    }
    
    
    @Test
    public void getEdgeTest() throws Exception
    {
        testPopulateGraph();
        
        // Populated undirected graph
        assertTrue(myGraph.getEdge("Z", "P") == null);
        
        // Empty, directed graph
        assertTrue(badGraph.getEdge("Z", "P") == null);
        
        assertTrue(myGraph.getEdge("A", "A") == null);
        
        assertTrue(myGraph.getEdge("A", "B").getDestination().equals("B"));
        assertTrue(myGraph.getEdge("B", "A").getDestination().equals("A"));
        assertTrue(myGraph.getEdge("A", "B").getSource().equals("A"));
        assertTrue(myGraph.getEdge("B", "A").getSource().equals("B"));
        assertTrue(myGraph.getEdge("A", "B").getWeight() == 10);
        assertTrue(myGraph.getEdge("B", "A").getWeight() == 10);
        
        assertTrue(myGraph.getEdge(null, null) == null);
    }
    
    
    @Test
    public void getEdgesTest() throws Exception
    {
        testPopulateGraph();
        
        // WE NEED TO VERIFY THAT ALL EDGES OF myGraph ARE CONTAINED IN THE EDGE SET
        
        // Also need directed graph...
        
        List<Graph.Edge<String>> myList = myGraph.getEdges();
        List<Graph.Edge<String>> emptyList = badGraph.getEdges();
        
        badGraph.addVertex("A");
        
        // Needs to be empty since only a single vertex and no edges are present
        assertTrue(emptyList.isEmpty());
        
        HashSet<Graph.Edge<String>> edgeSet = new HashSet<>(myGraph.getEdges());
        
        for (Graph.Edge<String> edge: myList)
        {
            assertTrue(myGraph.edgeExists(edge.getSource(), edge.getDestination()));
            assertTrue(myGraph.edgeExists(edge.getSource(), edge.getDestination()));
        }
    }
    
    
    @Test
    public void minimumSpanningTreeTest() throws Exception 
    {
        // Populates the graph
        testPopulateGraph();
        
        Graph<String> minTree = myGraph.minimumSpanningTree();
        
        Graph<String> pMinTree = pGraph.minimumSpanningTree();
        
        try
        {
            badGraph.minimumSpanningTree();
            
            fail("Should not work for directed graphs!");
        }
        catch (IllegalStateException ise)
        {
            // Success!!!
        }

//        minTree.addEdge("D", "F", 7);

        System.out.println(minTree);

        // The edges contained in the minimum spanning tree
        assertTrue(minTree.edgeExists("A", "D"));
        assertTrue(minTree.edgeExists("D", "A"));
        assertTrue(minTree.edgeExists("D", "F"));
        assertTrue(minTree.edgeExists("F", "D"));
        assertTrue(minTree.edgeExists("F", "E"));
        assertTrue(minTree.edgeExists("E", "F"));
        assertTrue(minTree.edgeExists("E", "B"));
        assertTrue(minTree.edgeExists("B", "E"));
        assertTrue(minTree.edgeExists("B", "C"));
        assertTrue(minTree.edgeExists("C", "B"));
        assertTrue(minTree.edgeExists("E", "G"));
        assertTrue(minTree.edgeExists("G", "E"));
        
        // The edges that are in the graph, but not in the minimum
        // spanning tree
        assertFalse(minTree.edgeExists("A", "C"));
        assertFalse(minTree.edgeExists("C", "A"));
        assertFalse(minTree.edgeExists("A", "F"));
        assertFalse(minTree.edgeExists("F", "A"));
        assertFalse(minTree.edgeExists("A", "B"));
        assertFalse(minTree.edgeExists("B", "A"));
        assertFalse(minTree.edgeExists("B", "D"));
        assertFalse(minTree.edgeExists("D", "B"));
        assertFalse(minTree.edgeExists("B", "G"));
        assertFalse(minTree.edgeExists("G", "B"));
        assertFalse(minTree.edgeExists("C", "F"));
        assertFalse(minTree.edgeExists("F", "C"));
        assertFalse(minTree.edgeExists("C", "E"));
        assertFalse(minTree.edgeExists("E", "C"));
        
        
        // For the pMinTree (the minimum spanning tree of Peyton's graph),
        // the edges contained in the minimum spanning tree
        assertTrue(pMinTree.edgeExists("E", "D"));
        assertTrue(pMinTree.edgeExists("D", "E"));
        assertTrue(pMinTree.edgeExists("D", "C"));
        assertTrue(pMinTree.edgeExists("C", "D"));
        assertTrue(pMinTree.edgeExists("C", "A"));
        assertTrue(pMinTree.edgeExists("A", "C"));
        assertTrue(pMinTree.edgeExists("A", "B"));
        assertTrue(pMinTree.edgeExists("B", "A"));
        assertTrue(pMinTree.edgeExists("B", "G"));
        assertTrue(pMinTree.edgeExists("G", "B"));
        assertTrue(pMinTree.edgeExists("G", "F"));
        assertTrue(pMinTree.edgeExists("F", "G"));
        assertTrue(pMinTree.edgeExists("F", "H"));
        assertTrue(pMinTree.edgeExists("G", "F"));
        assertTrue(pMinTree.edgeExists("H", "J"));
        assertTrue(pMinTree.edgeExists("J", "H"));
        assertTrue(pMinTree.edgeExists("J", "I"));
        assertTrue(pMinTree.edgeExists("I", "J"));
        assertTrue(pMinTree.edgeExists("I", "K"));
        assertTrue(pMinTree.edgeExists("K", "I"));
        
        // The edges present in pGraph
        assertFalse(pMinTree.edgeExists("C", "E"));
        assertFalse(pMinTree.edgeExists("E", "C"));
        assertFalse(pMinTree.edgeExists("D", "F"));
        assertFalse(pMinTree.edgeExists("F", "D"));
        assertFalse(pMinTree.edgeExists("C", "E"));
        assertFalse(pMinTree.edgeExists("E", "C"));
        assertFalse(pMinTree.edgeExists("A", "F"));
        assertFalse(pMinTree.edgeExists("F", "A"));
        assertFalse(pMinTree.edgeExists("E", "H"));
        assertFalse(pMinTree.edgeExists("H", "E"));
        assertFalse(pMinTree.edgeExists("J", "K"));
        assertFalse(pMinTree.edgeExists("K", "J"));
        assertFalse(pMinTree.edgeExists("I", "G"));
        assertFalse(pMinTree.edgeExists("G", "I"));
        assertFalse(pMinTree.edgeExists("K", "G"));
        assertFalse(pMinTree.edgeExists("G", "K"));
    }
    
    @Test
    public void edgeCompareTest() throws Exception
    {
        testPopulateGraph();
        
        // e1 has weight 3
        Graph.Edge<String> e1 = pGraph.getEdge("A", "B");
        
        // e2 has weight 2
        Graph.Edge<String> e2 = pGraph.getEdge("A", "C");
        
        assertTrue(e1.compareTo(e2) > 0);
        assertTrue(e2.compareTo(e1) < 0);
        assertTrue(e1.compareTo(e1) == 0);
    }
    
    
    @Test
    public void toStringTest() throws Exception 
    {
        _testDirectedGraph.toString();
    } // MIGHT NOT NEED THIS...

    
    @Test
    public void testPopulateGraph() throws Exception
    {
        _testDirectedGraph.addVertex(a);
        _testDirectedGraph.addVertex(b);
        _testDirectedGraph.addVertex(c);
        _testUndirectedGraph.addVertex(a);
        _testUndirectedGraph.addVertex(b);
        _testUndirectedGraph.addVertex(c);
        
        _testDirectedGraph.addEdge(a, b, 0);
        _testDirectedGraph.addEdge(b, c, 0);
        _testUndirectedGraph.addEdge(a, b, 0);
        _testUndirectedGraph.addEdge(b, c, 0);
        
        pGraph.addVertex("A");
        pGraph.addVertex("B");
        pGraph.addVertex("C");
        pGraph.addVertex("D");
        pGraph.addVertex("E");
        pGraph.addVertex("F");
        pGraph.addVertex("G");
        pGraph.addVertex("H");
        pGraph.addVertex("I");
        pGraph.addVertex("J");
        pGraph.addVertex("K");

        pGraph.addEdge("A", "B", 3);
        pGraph.addEdge("A", "C", 2);
        pGraph.addEdge("A", "F", 5);
        pGraph.addEdge("B", "G", 3);
        pGraph.addEdge("C", "D", 1);
        pGraph.addEdge("C", "E", 2);
        pGraph.addEdge("D", "E", 1);
        pGraph.addEdge("D", "F", 6);
        pGraph.addEdge("E", "H", 4);
        pGraph.addEdge("F", "H", 2);
        pGraph.addEdge("F", "G", 2);
        pGraph.addEdge("G", "I", 5);
        pGraph.addEdge("G", "K", 3);
        pGraph.addEdge("H", "J", 1);
        pGraph.addEdge("I", "J", 1);
        pGraph.addEdge("I", "K", 1);
        pGraph.addEdge("J", "K", 3);

        myGraph.addVertex("A");
        myGraph.addVertex("B");
        myGraph.addVertex("C");
        myGraph.addVertex("D");
        myGraph.addVertex("E");
        myGraph.addVertex("F");
        myGraph.addVertex("G");

        myGraph.addEdge("A", "B", 10);
        myGraph.addEdge("A", "C", 8);
        myGraph.addEdge("A", "F", 8);
        myGraph.addEdge("A", "D", 4);
        myGraph.addEdge("B", "D", 18);
        myGraph.addEdge("B", "C", 4);
        myGraph.addEdge("B", "E", 6);
        myGraph.addEdge("B", "G", 13);
        myGraph.addEdge("C", "E", 12);
        myGraph.addEdge("C", "F", 11);
        myGraph.addEdge("D", "B", 18);
        myGraph.addEdge("D", "F", 7);
        myGraph.addEdge("E", "F", 5);
        myGraph.addEdge("E", "G", 12);
    }
}
