For my algorithms course I developed a simple templated graph class. It does not allow loops, null vertexes, or multiple edges between a specific source and destination. Each edge has with it a non negative integer weight. The class was used to learn and demonstrate concepts in computer science by including a number of algorithms, including Dijkstra's shortest path, a genetic algorithm for the TSP, etc. During this time, types of algorithms were studied (e.g. greedy, divide and conquer, etc.), along with time complexity and graph theory.


# Developer Info

Created by Peyton Hanel
